package com.example.vivijobsheet13

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    private lateinit var NamaEdit: EditText
    private lateinit var KelasEdit: EditText
    private lateinit var NisEdit: EditText
    private lateinit var SubmitBtn: Button
    private val NAME_KEY: String = "nama"
    private val CLASS_KEY: String = "kelas"
    private val NIS_KEY: String = "nis"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        NamaEdit = findViewById(R.id.namaEdit)
        KelasEdit = findViewById(R.id.kelasEdit)
        NisEdit = findViewById(R.id.NisEdit)
        SubmitBtn = findViewById(R.id.submitBtn)
        SubmitBtn.setOnClickListener {
            val intent = Intent(this, MainActivity2::class.java)
            intent.putExtra(NAME_KEY, NamaEdit.text.toString())
            intent.putExtra(CLASS_KEY, KelasEdit.text.toString())
            intent.putExtra(NIS_KEY, NisEdit.text.toString())
            startActivity(intent)
        }
    }
}